/*
 * File: colon.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __COLON_H__
#define __COLON_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void eml_signed_integer_colon(int b, emxArray_int32_T *y);

#endif

/*
 * File trailer for colon.h
 *
 * [EOF]
 */
