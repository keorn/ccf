/*
 * File: growManyTreesCompileTest_data.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

#ifndef __GROWMANYTREESCOMPILETEST_DATA_H__
#define __GROWMANYTREESCOMPILETEST_DATA_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Variable Declarations */
extern boolean_T state_not_empty;

#endif

/*
 * File trailer for growManyTreesCompileTest_data.h
 *
 * [EOF]
 */
