/*
 * File: rand.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __RAND_H__
#define __RAND_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern double b_rand(void);
extern void c_rand(double varargin_1, emxArray_real_T *r);

#endif

/*
 * File trailer for rand.h
 *
 * [EOF]
 */
