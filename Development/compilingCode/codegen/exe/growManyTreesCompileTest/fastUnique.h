/*
 * File: fastUnique.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __FASTUNIQUE_H__
#define __FASTUNIQUE_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void fastUnique(const emxArray_real_T *X, emxArray_real_T *uX);

#endif

/*
 * File trailer for fastUnique.h
 *
 * [EOF]
 */
