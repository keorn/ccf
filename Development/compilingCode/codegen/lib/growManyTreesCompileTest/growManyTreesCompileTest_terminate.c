/*
 * File: growManyTreesCompileTest_terminate.c
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

/* Include files */
#include "rt_nonfinite.h"
#include "growManyTreesCompileTest.h"
#include "growManyTreesCompileTest_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void growManyTreesCompileTest_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for growManyTreesCompileTest_terminate.c
 *
 * [EOF]
 */
