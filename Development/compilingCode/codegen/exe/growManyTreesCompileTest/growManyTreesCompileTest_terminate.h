/*
 * File: growManyTreesCompileTest_terminate.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __GROWMANYTREESCOMPILETEST_TERMINATE_H__
#define __GROWMANYTREESCOMPILETEST_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void growManyTreesCompileTest_terminate(void);

#endif

/*
 * File trailer for growManyTreesCompileTest_terminate.h
 *
 * [EOF]
 */
