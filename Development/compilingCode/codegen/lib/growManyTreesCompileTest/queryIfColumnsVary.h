/*
 * File: queryIfColumnsVary.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

#ifndef __QUERYIFCOLUMNSVARY_H__
#define __QUERYIFCOLUMNSVARY_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void queryIfColumnsVary(const emxArray_real_T *XvarToTest, double tol,
  emxArray_boolean_T *bVar);

#endif

/*
 * File trailer for queryIfColumnsVary.h
 *
 * [EOF]
 */
