/*
 * File: eml_rand_mt19937ar_stateful.c
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:37:59
 */

/* Include files */
#include "rt_nonfinite.h"
#include "growManyTreesCompileTest.h"
#include "eml_rand_mt19937ar_stateful.h"
#include "growManyTreesCompileTest_data.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void state_not_empty_init(void)
{
  state_not_empty = false;
}

/*
 * File trailer for eml_rand_mt19937ar_stateful.c
 *
 * [EOF]
 */
