/*
 * File: any.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __ANY_H__
#define __ANY_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T any(const boolean_T x[2]);
extern void b_any(const emxArray_boolean_T *x, emxArray_boolean_T *y);
extern void c_any(const emxArray_boolean_T *x, emxArray_boolean_T *y);
extern boolean_T d_any(const emxArray_boolean_T *x);

#endif

/*
 * File trailer for any.h
 *
 * [EOF]
 */
