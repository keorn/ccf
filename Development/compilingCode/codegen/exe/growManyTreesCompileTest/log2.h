/*
 * File: log2.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __LOG2_H__
#define __LOG2_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void b_log2(const emxArray_real_T *x, emxArray_real_T *f);

#endif

/*
 * File trailer for log2.h
 *
 * [EOF]
 */
