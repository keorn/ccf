/*
 * File: svd.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __SVD_H__
#define __SVD_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern void svd(const emxArray_real_T *A, emxArray_real_T *U, emxArray_real_T *S,
                emxArray_real_T *V);

#endif

/*
 * File trailer for svd.h
 *
 * [EOF]
 */
