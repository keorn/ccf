/*
 * File: strcmpi.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __STRCMPI_H__
#define __STRCMPI_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T b_strcmpi(const char s1_data[], const int s1_size[2]);
extern boolean_T c_strcmpi(const char s1_data[], const int s1_size[2]);
extern boolean_T d_strcmpi(const char s1_data[], const int s1_size[2]);
extern boolean_T e_strcmpi(const char s1_data[], const int s1_size[2]);

#endif

/*
 * File trailer for strcmpi.h
 *
 * [EOF]
 */
