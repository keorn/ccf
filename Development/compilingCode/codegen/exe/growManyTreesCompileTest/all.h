/*
 * File: all.h
 *
 * MATLAB Coder version            : 2.6
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07
 */

#ifndef __ALL_H__
#define __ALL_H__

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T all(const emxArray_boolean_T *x);
extern void b_all(const emxArray_boolean_T *x, emxArray_boolean_T *y);
extern boolean_T c_all(const emxArray_boolean_T *x);

#endif

/*
 * File trailer for all.h
 *
 * [EOF]
 */
