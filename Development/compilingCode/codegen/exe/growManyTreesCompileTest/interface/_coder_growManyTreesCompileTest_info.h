/* 
 * File: _coder_growManyTreesCompileTest_info.h 
 *  
 * MATLAB Coder version            : 2.6 
 * C/C++ source code generated on  : 28-Jul-2015 12:13:07 
 */

#ifndef ___CODER_GROWMANYTREESCOMPILETEST_INFO_H__
#define ___CODER_GROWMANYTREESCOMPILETEST_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* 
 * File trailer for _coder_growManyTreesCompileTest_info.h 
 *  
 * [EOF] 
 */
