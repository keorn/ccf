/*
 * queryIfOnlyTwoUniqueRows.h
 *
 * Code generation for function 'queryIfOnlyTwoUniqueRows'
 *
 */

#ifndef __QUERYIFONLYTWOUNIQUEROWS_H__
#define __QUERYIFONLYTWOUNIQUEROWS_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "growManyTreesCompileTest_types.h"

/* Function Declarations */
extern boolean_T queryIfOnlyTwoUniqueRows(const emlrtStack *sp, const
  emxArray_real_T *X);

#endif

/* End of code generation (queryIfOnlyTwoUniqueRows.h) */
